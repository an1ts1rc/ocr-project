﻿using System;
using Tesseract;
using System.IO;

namespace OcrConsoleApp_V1
{
    class Program
    {
        static void Main(string[] args)
        {
            var ENGLISH_LANGUAGE = @"eng";
            var tessdataDir = @"C:\Users\YCS-X2\Desktop\OCR Project\OcrConsoleApp_V1\OcrConsoleApp_V1\tessdata";
            var blogPostImage = @"C:\Users\YCS-X2\Desktop\YCS\Output-logs\XmlOut Caribbean Hol\XmlOut\test.jpg";

            // Inicia el conteo del reloj
            var watch = System.Diagnostics.Stopwatch.StartNew();

            /////////////////////////////////////////////////////////////////////////////////
            //       _____  _____  ____   ____   _____  ____      _      ____  _____       //
            //      |_   _|| ____|/ ___| / ___| | ____||  _ \    / \    / ___||_   _|      //
            //        | |  |  _|  \___ \ \___ \ |  _|  | |_) |  / _ \  | |      | |        //
            //        | |  | |___  ___) | ___) || |___ |  _ <  / ___ \ | |___   | |        //
            //        |_|  |_____||____/ |____/ |_____||_| \_\/_/   \_\ \____|  |_|        //
            //                                                                             //
            /////////////////////////////////////////////////////////////////////////////////
                                                                          
             
            using (var ocrEngine = new TesseractEngine(tessdataDir, ENGLISH_LANGUAGE, EngineMode.Default))
            {
                using (var imageWithText = Pix.LoadFromFile(blogPostImage))
                {
                    using (var page = ocrEngine.Process(imageWithText, PageSegMode.SingleBlock))
                    // Page Segmentation Method (PageSegMode): Single Block
                    // Assume a single uniform block of text.
                    {
                        string text = page.GetText();
                        File.WriteAllText(@"C:\YCS\YcsFiscal\XmlOut\ocrOutTest.txt", text);

                        // Detiene el conteo del reloj 
                        watch.Stop();

                        // Tiempo transcurrido
                        var elapsedSeconds = watch.Elapsed.TotalSeconds;
                        Console.WriteLine("Elapsed Time: " + elapsedSeconds + "seconds");
                        Console.ReadLine();
                    }
                }
            }
        }
    }
}
